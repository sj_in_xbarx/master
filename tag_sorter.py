#-*- coding:utf-8 -*-
from __future__ import unicode_literals

from .tag_sets import PrdTags, MenuTags



def del_substring(texts) : 
    super_strings = []
    sub_strings = []
    for text in sorted(texts, key=len, reverse=True) :
        if text not in ''.join(super_strings) : 
            super_strings.append(text)
        else : 
            sub_strings.append(text)
    return [text for text in texts if text not in sub_strings]


def rreplace(s, old, new, occurrence):
    li = s.rsplit(old, occurrence)
    return new.join(li)


class TagSorter(object) : 

    def add_on_tag(self) : 
        # add_on과 add_on의 add_on을 추가하기    
        for i in range(2) : 
            for tag in self.taglist :
                for add_on in tag.add_on.split(";") : 
                    if add_on != "" and add_on not in [tag.name for tag in self.taglist] : 
                        self.taglist += [tag_ for tag_ in self.cls()() if tag_.name == add_on]

    def sort_by_after(self) : 

        def locate_forward(move_tag_A, after_tag_B, taglist) : 
            # 주어진 태그리스트에서 add_on이나 after가 있는 경우, 해당 태그보다 뒤에 위치하게 하기 (list 상에 앞에 위치해 먼저 처리되도록)
            A_locate = [tag_.name for tag_ in taglist].index(move_tag_A.name)
            B_locate = [tag_.name for tag_ in taglist].index(after_tag_B)
            if A_locate > B_locate : 
                taglist.insert(B_locate, taglist.pop(A_locate))
            return taglist

        def _sort_by_after(taglist) : 
            # after를 처리할 게 서로 chain으로 연결되어있을 수 있으므로 두번 처리해줌 (A를 B 뒤에 보내려 했으나 B가 C 뒤로 갈 경우 A,B,C  -> B,A,C -> A,C,B 문제)
            for tag in taglist : 
                after_them = [after_ for after_ in tag.after.split(';') + tag.add_on.split(';') if after_ != '' and after_ in [tag_.name for tag_ in taglist]]
                for after in after_them : 
                    taglist = locate_forward(tag, after, taglist)
            return taglist

        for i in range(2) : 
            self.taglist = _sort_by_after(self.taglist)

    def convert_tag_type(self) : 
        # tag가 함께오는 다른 tag에 따라서 tag type을 변환함
        converted_index = []
        index_on_tags = 0
        for tag in self.taglist : 
            for i, j in [(tag.friend, 0), (tag.enemy,3)] : 
                if i != '' : 
                    for related_tag in i.split(";") : 
                        # freind나 enemy가 다른 태그 가운데 있거나, 자기자신을 제외한 다른 태그의 core에 있으면 태그 타입을 변경
                        if related_tag in [tag_.name for tag_ in self.taglist] + [tag_.core for tag_ in self.taglist if tag_.core != '' and tag_.name != tag.name] : 
                            self.taglist[index_on_tags] = tag._replace(type=j)
                            converted_index.append(index_on_tags)
            index_on_tags += 1
        # friend나 enemy가 있어서 type이 바뀐 경우 리스트 맨 뒤로 이동 >> 후순위태그
        for i in list( set(converted_index) ): 
            self.taglist.append(self.taglist.pop(i))

    def set_cate(self) :
        # 태그 타입 순서대로 상품 카테고리에 배정하기 
        cate_types = [[4], [1, 0], [5]]
        cate_list = [tag for tag in self.taglist if tag.core or tag.type == 0]
        for cate_type in cate_types : 
            cate_tag = [tag for tag in cate_list if tag.type in cate_type]
            if cate_tag : 
                cate_tag = cate_tag[0]
                # 제조사, 브랜드는 core가 분류와 상품종류가 되게 하기
                if cate_tag.type in [4, 5] :
                    self.product_cate = cate_tag.core
                    self.product_type.append(cate_tag.core)
                #  복합명사의 core 이외 나머지 부분은 나중에 주속성으로 추가함
                elif cate_tag.type == 1 :
                    self.product_cate = cate_tag.core
                # 0번 핵심어근 태그는 name이 분류와 상품종류가 되게 하기
                elif cate_tag.type == 0 :                     
                    self.product_cate = cate_tag.name
                    self.product_type.append(cate_tag.name)
                break

    def set_prdtype(self) : 
        # 상품종류에 속할 수 있는 모든 정보들을 추출하기
        def extract_typeinfo(_tag) : 
            # print(_tag)
            info_set = []
            if _tag.type in [0, 2] : 
                info_set.append(_tag.name)
            elif _tag.type == 1 :
                info_set = []
                # 1번 태그와 같이 core가 name에 없는 경우는 상품종류에 추가하지 않음 (4, 5번 태그가 core가 된 경우는 다른 부분을 통해 추가됨) 
                if  _tag.core : 
                    if _tag.core in _tag.name: 
                        info_set.append(_tag.core)
                    attr = _tag.name.rsplit(_tag.core, 1)
                    if attr :
                        # 나머지부분 가운데 core의 앞에 있는 것은 주속성으로, 뒤에 있는 것은 접미사로 넣기  
                        info_set.insert(0, attr[0])
                        if len(attr) == 2 : 
                            info_set.append(attr[1])
            return info_set

        # 0번과 1번 태그로부터 추출한 정보를 순서대로 리스트로 만들고, 중복을 제거한 리스트로 만들기
        # taglist가 최초에 거꾸로의 순서로 있었으므로 insert를 통해 순서를 바꿔주나, 각 info set는 순서가 유지되어야 함
        prdtype_tmp = self.product_type
        prdtype_tmp += [','.join(extract_typeinfo(tag)) for tag in self.taglist]
        # print(prdtype_tmp)
        prdtype_list = []
        for item_list in prdtype_tmp : 
            # info_set에 복수가 있었던 경우 순서를 유지하여 넣기
            if ',' in item_list : 
                item_list = item_list.split(',')[::-1]
                for item in item_list : 
                    if item and item not in prdtype_list : 
                        prdtype_list.insert(0, item)
            elif item_list and item_list not in prdtype_list : 
                prdtype_list.insert(0, item_list)
        # print(prdtype_list)
        # 접두사와 접미사를 각각 앞과 뒤에 붙이기, 태그리스트는 순서가 뒤집어져있으므로 다시 돌려서 처리하기
        prdtype_list = prdtype_list + [tag.name for tag in self.taglist if tag.type == 9][::-1]
        self.product_type = ''.join(prdtype_list)
        # 추출된 정보에서 product_cate를 뺀 나머지를 주속성으로 순서대로 넣기
        try : 
            prdtype_list.remove(self.product_cate)
        except : 
            pass
        self.main_attr = prdtype_list

    def change_synonym(self) :   
        # 11번 태그로 상품종류와 core를 교체
        synonym = [tag for tag in self.cls()() if tag.type in self.cls.synonym_type_for_final and tag.name == self.product_type]
        if synonym != [] : 
            synonym = synonym[0]
            if synonym.replace_to != '' : 
                self.product_type = synonym.replace_to
                if synonym.core != 'None' and synonym.core != '' :            
                    self.product_cate = synonym.core
                    attr = synonym.replace_to.replace(synonym.core, '')
                    if attr : 
                        self.main_attr = [attr]
            elif synonym.type == 0 :
                # 0번 태그를 synonym change type으로 설정한 경우 : 0번 태그 이름이 category가 됨 (메뉴 등 복합명사가 core가 cate가 되어야 하는 경우)
                self.product_cate = synonym.name

    def sort(self) : 
        self.add_on_tag()
        self.sort_by_after()
        self.convert_tag_type()
        self.set_cate()
        self.set_prdtype()
        for tag in self.taglist :
            self.sort_by_type(tag)
        self.change_synonym()
        return self.__dict__


class ProductTagSorter(TagSorter) : 

    def __init__(self, taglist) : 
        self.cls = PrdTags

        self.taglist = taglist
        
        self.product_cate = ''
        self.product_type = []
        self.main_attr = []
        self.character = []
        self.brand = []
        self.manufacturer = []
        self.quality = []
        self.keep_condition = []
        self.suffix = []
        self.origin = []

        self.type_activated = 0

    def sort_by_type(self, tag) :       # 태그마다 각각의 type에 맞춰 key에 넣기 
        type_key_map = {#0 : self.product_type,
                        #2 : self.main_attr, 
                        3 : self.character, 
                        4 : self.brand, 
                        5 : self.manufacturer, 
                        6 : self.quality, 
                        7 : self.keep_condition, 
                        8 : self.origin,
                        #9 : self.suffix,
                        }

        if tag.type in [4, 5, 8] :
            if tag.name not in type_key_map[tag.type] :
                type_key_map[tag.type].insert(0, tag.name)

        elif tag.type in [3, 6] or (tag.type == 7 and tag.name not in ['상온','실온']) : 
            if tag.name not in type_key_map[tag.type] :
                type_key_map[tag.type].append(tag.name)


class MenuTagSorter(TagSorter) : 

    def __init__(self, taglist) : 
        self.cls = MenuTags

        self.taglist = taglist
        
        self.product_cate = ''
        self.product_type = []
        self.main_attr = []
        self.character = []
        self.suffix = []
        self.utensil = []
        self.style = [] 

        self.sort()

    def sort_by_type(self, tag) :       # 태그마다 각각의 type에 맞춰 key에 넣기 
        type_key_map = {0 : self.product_type,
                        2 : self.main_attr, 
                        3 : self.character, 
                        4 : self.suffix, 
                        5 : self.utensil, 
                        6 : self.style, 
                        }
        if tag.type in [2, 3, 4, 5, 6] :
            if tag.name not in type_key_map[tag.type] :
                type_key_map[tag.type].insert(0, tag.name)