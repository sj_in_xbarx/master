#-*- coding:utf-8 -*-
from __future__ import unicode_literals
import re

from . import tagtools
from .tagtools import Extractor
from .tag_sets import Measures, PrdTags
from .measure_sorter import ProductMeasureSorter


# delete or replace special characters or strings used only for operation
def replace_special_chars(rawname) : 
    chars_to_delete = Measures.filter_name_by_type(type_='delete')
    for chars in chars_to_delete : 
        # '[커스]' 등 꺾은괄호나 괄호로 시작하는 것은 앞의 [, ( 와 뒤의 ], )를 escape 처리하기
        if chars[0] == '[' or chars[0] == '(' : 
            chars = '\\' + chars[:-1] + '\\' + chars[-1]
        rawname = re.sub(r'(?:%s)' % chars, '', rawname)
    rawname = Measures.replace_to(texts=[rawname], type_=['specials'])[0]
    return rawname 


# delete delivery information and expiration date
def delete_delivery(rawname) :
    # 김포D-3 등과 같이 지명이 같이 오는 것도 함께 제거하기, 
    # ,D,만 있거나 ,D-, 만 있는 것도 지우기 (배송일 잘린 것)
    # 유통기한8일 등과 같이 유통기한과 일수가 같이 오는 것도 제거하기 
    delivery = Measures.filter_name_by_type(type_='delivery')
    delivery = [tag_name.upper() for tag_name in delivery]
    regexes = Extractor(keywords= ['[가-힣]*D-[\d]*'],
                        avoid_front=['kor','eng','num'],
                        avoid_back=[])
    regexes += Extractor(keywords= [',D-?,',
                                   '(?:%s)[+0-9일]*' % '|'.join(delivery)],
                        avoid_front=[],
                        avoid_back=[])
    return regexes.del_and_gather(rawname.upper())


def find_to_keep(name) :          # 3M 등 measure가 포함된 제조사나 브랜드를 먼저 찾고 살려두기
    except_ = Measures.filter_name_by_type(type_='except')
    return Extractor(keywords='((?:%s))' % '|'.join(except_), 
                     avoid_front=['num'], 
                     avoid_back=['eng']).del_and_gather(name)
 

#####################################
##########    pipeline    ###########
#####################################


def find_volume(name) :         # 용량중량을 찾고 소거하고 동의어 치환하기
    forward, backward = Measures.bring_forward_backward()
    volumes = Measures.filter_name_by_type(type_='volume')
    regexes = Extractor(keywords="((?:실중량|고형량)[\s]?[\.0-9]+(?:kg|g))", 
                        avoid_front=['num', 'eng'],
                        avoid_back=['eng'])
    regexes += Extractor(keywords=volumes,
                         join=True,
                         forward=forward, 
                         backward=backward,
                         avoid_front=['num','kor','eng','.'], 
                         avoid_back=['num','eng'])
    regexes += Extractor(keywords= "[\d]+[\.][\d]+(?:%s)" % '|'.join(volumes),
                         avoid_front=['num'],
                         avoid_back=['eng']
                         )
    regexes += Extractor(keywords= "[\d]+(?:%s)" % '|'.join(volumes),
                         avoid_front=['num'],
                         avoid_back=['eng']
                         )
    regexes += Extractor(keywords="((?:kg|g))",
                         avoid_front=['num', 'eng', 'kor'], 
                         avoid_back=['num', 'eng', 'kor'])
    name, volume = regexes.del_and_gather(name)
    volume = [tagtools.trim_volume(v) for v in volume]
    if len(volume) >= 2 and 'kg' in volume : 
        volume.remove('kg')
    return name, list(set(Measures.replace_to(texts=volume, type_=['volume', 'forward'])))


def find_pound(name) :          # pound, lb 등 파운드 찾기 (size와 병합할 것)
    pound_tags = tagtools.join_to_regex(Measures.filter_name_by_type(type_='pound'))
    keywords = ['([0-9]+[~∼/\-][0-9]+(?:ea|개)/' + pound_tags + ')',    # n-n개/파운드 형태 찾기
                '([0-9]+' + pound_tags + ")",    # 숫자와 파운드가 오는 경우 찾기
                ]
    name, pounds = Extractor(keywords=keywords, 
                             avoid_front=['num'],
                             avoid_back=['eng', 'kor']).del_and_gather(name)
    pounds = [pound.replace('ea', '개') for pound in pounds]
    return name, Measures.replace_to(texts=pounds, type_=['pound'])


def find_unit(name) :           # 주문수량, 개체수량을 찾고 소거하기
    forward, backward = Measures.bring_forward_backward()
    units = Measures.filter_name_by_type(type_='unit')
    regexes = Extractor(keywords=units,
                        join=True,
                        forward=forward, backward=backward,
                        avoid_front=['num','kor'], avoid_back=['kor','num'])
    regexes += Extractor(keywords='[0-9]+(?:{0})'.format('|'.join(units)),
                        avoid_front=['num'], avoid_back=['kor','num'])    
    regexes += Extractor(keywords=Measures.filter_name_by_type(type_='unit_with_no_number'),
                         join=True,
                         avoid_front=['num', 'eng', 'kor'], 
                         avoid_back=['num', 'eng', 'kor'])
    name, unit = regexes.del_and_gather(name)
    return name, Measures.replace_to(texts=unit, type_=['unit'])
    

def find_grade(name) :          # 등급 찾기
    # 등급에 한글문자는 들어가면 안됨 --> 한글문자 포함 원할 시 type의 6번태그 권장
    grades = Measures.filter_name_by_type(type_='grade')
    regexes = Extractor(keywords=grades,
                        join=True)
    regexes += Extractor('[a-c0-9+]+등급')
    regexes.avoid(front=['num','eng'], back=['eng','kor','num', '\-', '\.'])
    name, grades = regexes.del_and_gather(name)
    return name, Measures.replace_to([grade.upper() for grade in grades], ['grade'])


def find_length(name) :        # cm, mm 등의 길이 표현 찾기
    forward, backward = Measures.bring_forward_backward()
    forward = tagtools.join_to_regex(forward + ['겉지방', '뼈길이','두께','엽'], '?[\s]?')
    body = tagtools.join_to_regex(Measures.filter_name_by_type(type_='length'))
    backward = tagtools.join_to_regex(backward + ['두께', '절단'], '?')
    
    regexes = Extractor("(" + forward + "?" + "[\s]?" 
                        + "[\.0-9]*[a-z*]*[\.0-9]*[*x×+~∼±/\-]*[\.0-9]*[Φφ*x+~∼±/\-]*[\.0-9]+" 
                        + body 
                        + backward + ")")    # 숫자의 범위가 함께 오는 것 찾기  
    regexes.avoid(front=['num','eng'],back=['eng'])
    name, length = regexes.del_and_gather(name)

    if len(length) == 2 : 
        if length[1].startswith('*') : 
            length = [''.join(length)]
        elif length[0].startswith('*') : 
            length = [''.join(length[::-1])]
    return name, Measures.replace_to(length, ['length'])


def find_nums_for_size(name) :      # 숫자가 특수기호와 함께 와서 규격이 되는 경우
    forward, backward = Measures.bring_forward_backward()
    percentile = Measures.filter_name_by_type(type_='percentile')
    product_cores = PrdTags.filter_name_by_type(type_=[0,1,3])            # 상품원형 뒤에 바로 숫자+% 가 오는 경우도 규격으로 잡기
    regexes = Extractor(keywords = ['[\.0-9]*[*x×+~∼/\-]?[\.0-9]+[*x×+~∼/\-][\.0-9]+[*x×+~∼±/\-]*[\d.]*미?',
                                '[0-9]+["미]',
                                '(?:{0})?[\s]?[\d]+[\%](?:호환가능)?(?:{1})?'.format('|'.join(percentile + product_cores), '|'.join(backward)),
                                '(?:{0})?[\s]?[\d]+[\.][\d]+[\%](?:호환가능)?(?:{1})?'.format('|'.join(percentile + product_cores), '|'.join(backward)),
                                ],
                         avoid_front=[], 
                         avoid_back=['eng'])
    # regexes += Extractor(keywords = ['(?:{0})[\.\d]+[\%](?:호환가능)?'.format('|'.join(percentile + product_cores)),
    #                                 '(?:{0})?[\s]?[\.\d]+[\%](?:호환가능)?'.format('|'.join(percentile)),
    #                                 ],
    #                     avoid_front=[],
    #                     avoid_back=['eng'])

    regexes += Extractor(keywords = '\d+/\d+',
                         avoid_front=['eng', 'kor', 'num'],
                         avoid_back=['eng', 'kor', 'num']
                         )
    name, size = regexes.del_and_gather(name)
    return name, size


def find_model(name) :   # 모델 번호 찾기    
    name, models = Extractor(keywords = ["(" + tagtools.join_to_regex(Measures.filter_name_by_type(type_='model')) + '[-]?[\.0-9]+[a-z]*[-]?[0-9]*' + ")",
                                        '([a-z]+[0-9]+[a-z]*-?[0-9a-z]*-?[0-9a-z]*)',  # KS993 같이 제품번호가 되는 것        
                                        '([a-z]+-[a-z0-9]+-?[0-9a-z]*)'],    # KS-993 같이 제품번호가 되는 것
                             avoid_front=['num', 'eng', 'kor'], 
                             avoid_back=['num', 'eng', 'kor']).del_and_gather(name, except_=['pdto', 'k1'])
    return name, [model.upper() for model in models]


def find_size(name) : # 규격 찾기    
    size_tags = Measures.filter_name_by_type(type_='size')
    forward = tagtools.join_to_regex(Measures.filter_name_by_type(type_='forward'), '?[\s]?')
    backward = tagtools.join_to_regex(Measures.filter_name_by_type(type_='backward'), '?')

    regexes = Extractor(keywords=size_tags,
                        join=True,
                        forward=forward, backward=backward,
                        avoid_front=['num','eng','kor'], avoid_back=['eng','kor'])
    # l, h, m 등 길이 높이 너비 등의 표기가 같이 오는 경우
    regexes += Extractor(keywords= '([\.0-9]*[*×xlwmha+~∼±/\'\-]+[\.0-9]+'
                                   + tagtools.join_to_regex(size_tags) 
                                   + backward + ")",
                         avoid_front=['num'],
                         avoid_back=['eng'])     
    regexes += Extractor(keywords= '([a-z가-힣]*[\.0-9]+[*×xlwmh+~∼±/\'\-]+[\.0-9]+'
                                   + tagtools.join_to_regex(size_tags) 
                                   + backward + ")",
                         avoid_front=['num'],
                         avoid_back=['eng'])
    regexes += Extractor(keywords="[0-9]+(?:%s)" % '|'.join(size_tags),
                         avoid_front=['num','eng'],
                         avoid_back=['eng']
                         )
    name, size = regexes.del_and_gather(name)
    return name, Measures.replace_to(size, ['size'])


def find_special_unit(name) :   # 숫자가 특수기호와 함께 와서 수량 등이 되는 경우
    regexes = Extractor(keywords='[*x()](\d+)',
                        avoid_front=['kor'],
                        avoid_back=['eng','kor','num'])
    regexes += Extractor(keywords='(\d+)[-~/(]',
                         avoid_front=['kor','num'],
                         avoid_back=['eng','kor','num'])
    name, units = regexes.del_and_gather(name)
    return name, [r+"개" for r in units]


def find_box(name) : 
    box_tags = tagtools.join_to_regex(Measures.filter_name_by_type(type_='box'))
    name, box = Extractor(keywords=['([0-9]*' + box_tags + ')',
                                    '(b|박스단위발주)'],
                          avoid_front=['eng','kor','num'],
                          avoid_back=['eng','kor','num']).del_and_gather(name)
    return name, Measures.replace_to(box, ['box'])


def find_pack(name) : 
    pack_tags = tagtools.join_to_regex(Measures.filter_name_by_type(type_='pack'))
    name, pack = Extractor(keywords=['(\d+' + pack_tags + ')',
                                     '(' + pack_tags + ')'],
                           avoid_front=['eng','kor','num'],
                           avoid_back=['eng','kor','num']).del_and_gather(name)
    return name, Measures.replace_to(pack, ['pack'])


def find_pipeline(rawname) : 
    name, volume = find_volume(rawname.lower())    
    name, pound = find_pound(name)
    name, unit = find_unit(name)
    name, grade = find_grade(name)
    name, length = find_length(name)
    name, nums_for_size = find_nums_for_size(name)
    name, model = find_model(name)
    name, size = find_size(name)
    name, special_unit = find_special_unit(name)
    name, box = find_box(name)
    name, pack = find_pack(name)
        
    unit = tagtools.del_duplicated_num(list(set(unit + special_unit)))
    size += pound + length + nums_for_size + model
    unit, pack, box = map(tagtools.del_one, [unit, pack, box])    
    if len(pack) >= 2 and '팩' in pack:
        pack.remove('팩')
    if len(unit) >= 2 and '개' in unit : 
        unit.remove('개')

    return {'rawname': rawname,
            'name': name.upper(),
            'unit': unit,
            'pack': pack,
            'box': box,
            'volume': volume,
            'size': size,
            'grade' : grade,
            }


# pipeline을 거쳐서 정보를 추출한 다음 sorting한 결과를 dictionary로 반환하는 main 함수
def find_measure(rawname) : 
    name, delivery = delete_delivery(replace_special_chars(rawname))
    name, keeped = find_to_keep(name)
    temp_measure = find_pipeline(name)
    if keeped : 
        temp_measure['name'] += ',' + ','.join(keeped)
    measure = ProductMeasureSorter(rawname).sort_measure(temp_measure['unit'],
                                                         temp_measure['box'],
                                                         temp_measure['pack'],
                                                         temp_measure['volume'],
                                                         temp_measure['size'],
                                                         )

    measure.update({'mid_name': temp_measure['name'],
                    'grade_': temp_measure['grade'],
                    'delivery': delivery,
                    })   
    return measure