#-*- coding:utf-8 -*-
from __future__ import unicode_literals
import re
import multiprocessing

from . import tag_sets
from . import prd_measure as measure
from . import prd_tagger as tagger


# change to master dictionary right after initiating, if the input is rawname of a product.
class ProductParser :       

    def __init__(self, rawname='') :
        self.rawname = rawname
        self.mid_name = rawname

        # make attribute from dictionary of found measures and, after define mid_name, find tags
        measure_found = measure.find_measure(self.rawname)
        for k,v in measure_found.items():
            setattr(self, k, v)
        # print(self.__dict__)

        tags_found = tagger.find_info(self.mid_name)
        for k,v in tags_found.items():
            setattr(self, k, v)
        self.quality += self.grade_
        # print(self.__dict__)
        
        self.setName()
        # print(self.__dict__)

        self.setFieldValue()

    # make summarized names such as appname, master_name, lineup_name
    def setName(self) : 
        self.master_name = ' '.join(self.manufacturer + self.brand + [self.product_type]).strip(' ')
        
        # 앱표시이름 만들 때 product_type이 brand name 안에 완전 속해있으면 앱표시는 brand로 대체
        if self.product_type in ''.join(self.manufacturer + self.brand) : 
            self.app_name = ' '.join(self.manufacturer + self.brand)
        else : 
            # 분류가 브랜드이름 안에 들어가있는 경우 앱표시이름에서 중복으로 나오지 않게 하기
            if self.product_cate in ' '.join(self.brand) : 
                self.app_name = ' '.join(self.manufacturer + self.brand + [self.product_type.replace(self.product_cate, '')]).strip(' ')
            else : 
                self.app_name = self.master_name
                
        # 앱표시이름 만들 때 (개체수량 * 개체중량) 형태가 가능하다면 내용량 자리에 오게하기 
        if self.ind_num != '' and self.ind_weight != '' : 
            self.app_name += ' ' + self.ind_weight + '*' + self.ind_num
        elif self.ind_weight and not self.ind_num : 
            self.app_name += ' ' + self.ind_weight
        elif self.net_contents != 'kg' and self.net_contents != '' : 
            self.app_name += ' ' + self.net_contents
        elif not self.net_contents and self.ind_num : 
            self.app_name += ' ' + self.ind_num

        # 괄호 안에 들어갈 내용 정하기
        adjectives = self.character + self.quality
        if self.standard and len(self.standard) <= 8 : 
            adjectives.append(self.standard)
        if adjectives : 
            self.app_name += '(' + ','.join(adjectives) + ')'

        # vws : volume, weight, size. 용량중량크기
        vws = [i for i in [self.ind_num, self.ind_weight, self.standard, self.rest] if i]
        if vws : 
            self.vws = ','.join(vws)
        else : 
            self.vws = None
        
        lineup = [self.net_contents, self.origin, ''.join(self.keep_condition), self.standard, self.ind_num, self.ind_weight, self.rest]
        self.lineup_name = '_'.join([i for i in lineup if i != ''])

    # if value is empty, set as None, list to string
    def setFieldValue(self) :    
        for k, v in self.__dict__.items() : 
            if not v : 
                setattr(self, k, None) 
            elif isinstance(v, list) : 
                setattr(self, k, ','.join(v)) 


# input rawname is string and tags should be list of namedtuple. return master dictionary            
def convert_masterproduct(rawname, product_tags=None, measure_tags=None) : 
    # if tags input exists, use. else, bring from DB of datatest server.
    tag_sets.PrdTags()(product_tags)
    tag_sets.Measures()(measure_tags)
    master = ProductParser(rawname)
    return master.__dict__

# input is list of rawnames, return list of dictionaries
def convert_bulk_rawnames(list_of_rawname, product_tags=None, measure_tags=None) : 
    tag_sets.PrdTags()(product_tags)
    tag_sets.Measures()(measure_tags)
    result = list(map(convert_masterproduct, list_of_rawname))
    return result    


# input should be dictionary, including 'rawname' as key. return updated dictionary.
def convert_masterproduct_from_dict(dict_input, product_tags=None, measure_tags=None) : 
    dict_input.update(convert_masterproduct(dict_input['rawname'], product_tags, measure_tags))
    return dict_input



##################################
###### USE ON PYTHON 3 ONLY ######
##################################

# input should be list of dictionaries, including 'rawname' as key. return list of dictionaries.
# use multi-processing (not multi-threading) to speed up.
def convert_masterproduct_bulk(list_of_dict, product_tags=None, measure_tags=None, processes=4) :    
    tag_sets.PrdTags()(product_tags)
    tag_sets.Measures()(measure_tags)
    if type(list_of_dict) == dict:
        list_of_dict = [list_of_dict]
    pool = multiprocessing.Pool(processes=processes)
    result = pool.map(convert_masterproduct_from_dict, list_of_dict)
    pool.close()
    return result
