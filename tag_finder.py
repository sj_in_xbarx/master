#-*- coding:utf-8 -*-
from __future__ import unicode_literals
import collections
import re
from operator import attrgetter

from .tag_sets import PrdTags, MenuTags


def len_of_letters(text) : # 특수문자를 제외하고 글자만의 길이를 세는 것
    length = 0
    for word in re.findall(r'[a-zA-Z가-힣0-9]+', text) : 
        if word : 
            length += len(word)
    return length


def del_back(target, tag) :  
# 뒤에서부터 찾아서 지우기
    return target[:target.rfind(tag)] + target[target.rfind(tag)+len(tag):]


def find_tempTag(target, tag_candidates) :  
    # 태그 후보를 찾아서 tempTag tuple로 만들기
    tempTag = collections.namedtuple('tempTag', ['tag', 'index', 'rindex', 'len'])
    if target :
        return [tempTag(tag=tag, 
                        index=target.find(tag.name), 
                        rindex=target[::-1].find(tag.name[::-1]), 
                        len=len(tag.name)) for tag in tag_candidates if tag.name in target]
    return []


def select_major(target, temp_tags_) :  
    # 가장 우선순위가 높은 태그 선정하고 상품에서 소거하기
    last = sorted(sorted(temp_tags_, key=attrgetter('len'), reverse=True),        # 맨 뒤에서 시작하면서 가장 긴 것
                  key=attrgetter('rindex'))[0]    

    first = sorted(sorted(temp_tags_, key=attrgetter('len'), reverse=True),       # 맨 앞에서 시작하면서 가장 긴 것
                   key=attrgetter('index'))[0]     
    
    if first.len + last.len > len_of_letters(target) :  # 두 태그의 길이의 합이 전체 길이보다 긴 경우
        if target.replace(last.tag.name,'') not in [temptag.tag.name for temptag in temp_tags_] : # 소거한 나머지가 태그에 없는 경우 
            return [target.replace(first.tag.name, "", 1), first] 
        elif first.len/(first.index*0.3 + 1) > last.len*2 : # 뒤에 나오는 태그에 더 가중치 두기
            return [target.replace(first.tag.name, "", 1), first]    
        else :
            return [del_back(target, last.tag.name), last]
            
    else : 
        if first.len/(first.index*0.3 + 1) >= last.len*1.5 :
            return [target.replace(first.tag.name, "", 1), first]    
        else :
            return [del_back(target, last.tag.name), last]


def add_on_tag(tag, tags_to_add) : 
    # 태그의 add_on을 반환하기
    adds = []
    for add_on in tag.add_on.split(";") : 
        if add_on != "" and add_on not in [tag_.name for tag_ in tags_to_add] : 
            adds += [tag_ for tag_ in PrdTags()() if tag_.name == add_on]
    return adds


class TagFinder(object) :

    def __init__(self, rawname, tagsetClass) : 
        self.rawname = rawname
        self.name = rawname

        self.prior_tags = []
        self.tags_found = []
        self.tagsetClass = tagsetClass
        self.tags = tagsetClass()()        
    
        self.find_prior_tags()

    def find_prior_tags(self) : 
        # 우선순위 높은 태그타입은 먼저 찾기
        if self.tagsetClass.prior_tags : 
            prior_tags_candidate = sorted( [tag_ for tag_ in self.tags if tag_.name in self.tagsetClass.prior_tags], key=lambda x : len(x.name), reverse=True )
            for tag_ in prior_tags_candidate : 
                # print(tag_, self.name)
                if tag_.name in self.name : 
                    self.name = self.name.replace(tag_.name, '')
                    self.prior_tags.append(tag_)
                

    def find_tag(self) : 
        # 태그를 찾는 가장 중요한 루틴 : 임시태그 찾고 가장 중요한 것 하나씩 선정 & 소거 반복

        def trim_name(name) : 
            # print name
            special_strs = [',', '/', '*', '#', '-']
            name = re.sub(r'^[%s]+' % '|'.join(special_strs), '', name)
            name = re.sub(r'[%s]+$' % '|'.join(special_strs), '', name)
            # print name
            return name

        # 마지막에 쓰이는 후순위 태그를 제외한 태그사전 만들기
        all_possible_tags = [tag_ for tag_ in self.tags if tag_.type not in self.tagsetClass.type_only_for_final]
        extracted_words = []
        # 문자열을 리스트로 나눈 다음 각 단어 내에서 태그 찾기
        for word in self.name.split(',') : 
            temp_tags = find_tempTag(trim_name(word), all_possible_tags)                    # 태그 후보 찾기
            temptag_index = {temptag.tag.name: temptag.rindex for temptag in temp_tags}     # 최초의 문자열에서의 위치 기록하기
            
            final_tags = []
            tags_to_add = []

            while len(temp_tags) >= 1 :     # 태그 후보가 남지 않을 때까지 태깅 & 소거 반복하기
                word, major = select_major(trim_name(word), temp_tags)
                # print word, major.tag, tags_to_add

                # 태그가 중복되지 않게 저장되게 하기
                if major.tag not in [_tag for _tag in self.tags_found] :                    
                    if major.tag.type not in [10, 11] :   
                        final_tags.append((major.tag, temptag_index[major.tag.name]))

                    else :  # 동의어 치환하기
                        try :
                            # replace 하기 전에 있는 add_on을 먼저 추가함
                            tags_to_add += add_on_tag(major.tag, tags_to_add)
                            major_replace_tag = [tag_ for tag_ in all_possible_tags if tag_.name == major.tag.replace_to][0]
                            final_tags.append((major_replace_tag, temptag_index[major.tag.name]))
                        except :
                            # print('tag called by replace_to is not exist : {}'.format(major.tag.name))
                            temp_tags.remove(major)
                            pass

                # 소거되고 남은 문자열에서 다시 임시태그를 찾기
                temp_tags = find_tempTag(word, [temp.tag for temp in temp_tags])

            # 소거되고 남은 문자열을 저장한 다음 나중에 self.name으로 합치기
            extracted_words.append(word)
            # 원래 문자열에서의 순서를 유지한 상태로 저장하기
            self.tags_found += [final_tag[0] for final_tag in sorted(final_tags, key=lambda element:element[1], reverse=False)] \
                             + [tag_ for tag_ in tags_to_add if tag_.name not in [tag__.name for tag__ in self.tags_found] ]

        self.tags_found += self.prior_tags
        self.name = ','.join(extracted_words)
        # print(self.name, self.tags_found)

        return (self.name, self.tags_found)



class ProductTagFinder(TagFinder):
    def __init__(self, rawname) : 
        super(ProductTagFinder, self).__init__(rawname, PrdTags)

class MenuTagFinder(TagFinder):
    def __init__(self, rawname) : 
        super(MenuTagFinder, self).__init__(rawname, MenuTags)