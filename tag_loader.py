#-*- coding:utf-8 -*-   
import collections, pymysql


def get_tags(item="tag") :     
    # print "\nBring tags : {}\n".format(item)
    tables = {"prdtag" : "xprd_master_tagset",
            "measure" : "xprd_master_tagset_measure",
            "menutag" : "xprd_master_menutag",
            }
    if item not in tables.keys() : 
        raise Exception    

    if True:
    # if False:    
        from django.db import connection
        cfg = connection.settings_dict
        conn = pymysql.connect(host=cfg['HOST'], port=int(cfg['PORT']), user=cfg['USER'], password=cfg['PASSWORD'], db=cfg['NAME'], charset='utf8')
    else:
        conn = pymysql.connect(host='datatest.koreacentral.cloudapp.azure.com', 
                                port=5000, 
                                user='ople', 
                                password='x_x@song2ro', 
                                db='prd_master2', 
                                charset='utf8')
   
    curs = conn.cursor(pymysql.cursors.DictCursor)
    sql = "SELECT * FROM {}".format(tables[item])
    curs.execute(sql)
    reader = curs.fetchall()
    conn.close()
    if not reader:
        return []
    for item in reader : 
        item.pop('create_date', None)
        item.pop('update_date', None)
    Tags = collections.namedtuple('Tags', reader[0].keys())
    return [Tags(**row) for row in reader]