#-*- coding:utf-8 -*-
from __future__ import unicode_literals
import collections
import re
from operator import itemgetter, attrgetter

from .tag_sets import PrdTags
from .tag_finder import ProductTagFinder
from .tag_sorter import ProductTagSorter
from . import tagtools
from .tagtools import Extractor


#####################################
##########    pre_process    ###########
#####################################

def _find_origin(name, regex_extractor) : 
    name, origins_found = regex_extractor.del_and_gather(name)
    # print(origins_found, name)
    origins_found = PrdTags.replace_to(origins_found, type_=[18, 'origin'])
    origins_found = [origin_.strip(',').replace('/', ',') for origin_ in origins_found if origin_ != ',']
    return name, origins_found    

def find_origin(name) :          # 원산지 태깅하기
    friends = PrdTags.select(PrdTags.filter_by_friend(friend='origin'), field='name')
    food_tags = PrdTags.select(PrdTags.filter(type_=[0, 1]), field='name')
    origin_tags = PrdTags.select(PrdTags.filter(type_=[8, 18]), field='name')
    
    # friend에 origin이 있는 태그를 포함하기
    regex = [
            # 연육:수입산(미국산,베트남산,인도산) 과 같은 케이스
            '((?:{friends})?(?:[가-힣]*:)?(?:{tags})\((?:{tags},?)+\))'.format(friends='|'.join(friends+food_tags), tags=',?|'.join(origin_tags)),            
            # 계육/돈육:미국산 등, 원산지 앞의 : 앞에 두 개 이상의 식품이 왔을 때 (단 1개만 왔을 때에는 , 등이 붙으면 안되도록)
            '((?:{0}/?,?)*(?:{1})(?:[가-힣]*:)(?:{2})[0-9]*)'.format('/?,?|'.join(friends), '|'.join(friends+food_tags), '|'.join(origin_tags)),
            # 계육:미국산,중국산 등 상품 뒤에 복수의 원산지가 붙는 경우
            '((?:{0})(?:[가-힣]*:)(?:{1})[,](?:{1})[0-9]*)'.format('/?,?|'.join(friends+food_tags), '|'.join(friends), '|'.join(origin_tags)),
            # 국내산(무,배추,고추가루) 등 원산지 뒤에 괄호 안에 식품을 넣는 경우
            '((?:{tags})\((?:{friends}/?,?)*\))'.format(friends='/?,?|'.join(friends+food_tags), tags='|'.join(origin_tags)),
            '((?:{0}))'.format('|'.join(origin_tags)),        
            ]
    
    regexes = Extractor(keywords=regex, 
                        avoid_front=['kor'],
                        avoid_back=['kor'])
    return _find_origin(name, regexes)


def find_character_by_pattern(name) :         # ~~용, ~~맛 (특징) 찾고 소거하기
    # 1회용 문제로 인해 1 만 키워드에 포함될 수 있도록 함
    regexes = Extractor(keywords='[1가-힣]+용',
                        avoid_front=['kor','eng','1'],
                        avoid_back=['kor'])
    pattern_keywords = ['향', '맛', '색', '제거X', '제거', '있음', '없음', '포함', '프리', '형', '별도']
    regexes += Extractor(keywords='((?:FD)?[가-힣]+(?:%s))' % '|'.join(pattern_keywords),
                         avoid_front=['kor','eng'],
                         avoid_back=['kor'])
    name, temp_purposes = regexes.del_and_gather(name, except_='쥬시쿨')
    # print(name, temp_purposes)
    # 위 패턴으로 끝나는 태그가 이미 있다면 패턴을 복구하기   
    exist_tags = [i for i in PrdTags.filter_name_by_type() if i in temp_purposes]
    restore = []
    sub_attr = []
    for temp in temp_purposes : 
        if temp in exist_tags and temp not in restore : 
            restore.append(temp)
        else : 
            brands = PrdTags.filter_name_by_type(type_=4)
            for b in brands : 
                if temp.startswith(b) : 
                    temp = temp.replace(b, '')
                    restore.append(b)
            if temp not in sub_attr : 
                sub_attr.append(temp)
    if restore : 
        name = ','.join([name] + restore)

    # 찾아진 정보를 동의어 치환하고 불용어와 중복을 제거하기
    purposes = []
    for purp in PrdTags.replace_to(texts=sub_attr, exact=True) : 
        if purp and purp not in PrdTags.filter_name_by_type(type_=99) + restore : 
            if purp not in purposes: 
                purposes.append(purp)

    return name, purposes



'''
#### post_process
'''

def find_origin_final(name, origins_found):  # 특수원산지 추가
    # special_origin = {'노':'노르웨이산','러':'러시아산'} 
    # for origin_ in [origin_ for origin_ in special_origin.keys() if origin_ in name] : 
    #     origins_found.append(special_origin[origin_])
    #     name.replace(origin_, '')
    origin_tags = PrdTags.select(PrdTags.filter(type_=[8, 18]), field='name')
    regex = Extractor(keywords='((?:{0}))'.format('|'.join(origin_tags)),
                      avoid_front=['kor'],
                      avoid_back=[]
                      )    
    name, origin_added = _find_origin(name, regex)
    origins_found += origin_added

    if len(origins_found)>= 2 and '외국산' in origins_found : 
        origins_found.remove('외국산')
    return name, ','.join(origins_found)


def trim_rest(name) : 
    # 소거하고 남은 상품명에서 의미 없는 특수문자를 기준으로 split해 rest 만들기
    sps = [".", "&", "*", "#", "-", "%", "~", "`"]
    regexStr = r'[a-zA-Z0-9가-힣%s]+' % "".join(map(lambda x: "\\%s"%x, sps))
    rest = ','.join(re.findall(regexStr, name))
    for s in ['.','*',',','&','-','~'] : 
        rest = rest.strip(s)
    return rest


'''
#### main
'''


def find_info(rawname) :
    # pre_process
    name, origin = find_origin(rawname.upper())
    # print(name, origin)
    name, character = find_character_by_pattern(name)
    # print(name, character)
    name = tagtools.reorder_name(name.replace(' ','').upper())

    # find and sort
    rest_name, tags_found = ProductTagFinder(name).find_tag()
    # print(tags_found)
    info_found = ProductTagSorter(tags_found).sort()

    # post_process
    info_found['character'] += character    
    name, info_found['origin'] = find_origin_final(rest_name, origin + info_found['origin'])
    info_found['rest'] = trim_rest(name)
    for key_to_del in ['taglist', 'suffix', 'cls']:
        info_found.pop(key_to_del, None)
    return info_found