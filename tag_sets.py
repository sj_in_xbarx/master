#-*- coding:utf-8 -*-   
from __future__ import unicode_literals
import collections
import re
from .tag_loader import get_tags


def replace_by_tuple(text, tuples, exact=False): 
    # dictionary 기반의 동의어 변환기
    if exact : 
        for i, j in tuples : 
            if i != '' and j != None : 
                if text == i : 
                    text = j
    else : 
        for i, j in tuples: 
            if j != '' and j != None : 
                text = text.replace(i, j)
    return text



##################
# 공통 기능
##################

class TagSets(object): 
    # singleton pattern. same object whenever called
    _instances = {}
    item = ""
    @classmethod
    def __call__(cls, instances=None) :
        ## instances를 넣으면 해당 인스턴스로 싱글톤이 대체되고, 그렇지 않으면 동일한 객체를 계속 사용    
        if instances : 
            cls._instances[cls] = instances
        if cls not in cls._instances: 
            cls._instances[cls] = get_tags(item=cls.item) 
        return cls._instances[cls]

    @classmethod
    def filter(cls, name_=[], type_=[], except_type=[]) : 
        filtered = []

        items = cls()()
        if name_ != [] : 
            filtered += [tag for tag in items if tag.name in name_]
        if type_ != [] : 
            filtered += [tag for tag in items if tag.type in type_]
        if name_ != [] and type_ != [] : 
            filtered = [tag for tag in items if tag.name in name_ and tag.type in type_]
        if name_ == [] and type_ == [] : 
            filtered = items

        if except_type != [] : 
            filtered = [tag for tag in filtered if tag.type not in except_type]
        return filtered

    @classmethod
    def filter_name_by_type(cls, type_='') : 
        if type_ == '' : 
            return [tag.name for tag in cls()()]
        elif isinstance(type_, list) : 
            return sorted([tag.name for tag in cls()() if tag.type in type_], key=lambda x : len(x), reverse=True)
        else : 
            return sorted([tag.name for tag in cls()() if tag.type == type_], key=lambda x : len(x), reverse=True)

    @classmethod
    def clear(cls) : 
        cls._instances = {}
        cls.cached = {}        


##################
# 수치정보분석기
##################

class Measures(TagSets) : 
    _instances = {}
    item = "measure"

    @staticmethod
    def select(taglist, field) : 
        if field == 'name' : 
            taglist = [tag.name for tag in taglist]
        elif field == 'type' : 
            taglist = [tag.type for tag in taglist]
        elif field == 'replace_to' : 
            taglist = [tag.replace_to for tag in taglist]
        return sorted(taglist, key=len, reverse=True)


    @classmethod
    def replace_to(cls, texts=[], type_=[]) : 
        if type_ == [] :
            replace_tuples = [(tag.name, tag.replace_to) for tag in cls()() if tag.replace_to != None]
        else : 
            replace_tuples = [(tag.name, tag.replace_to) for tag in cls()() if tag.replace_to != None and tag.type in type_]
        replace_tuples = sorted(replace_tuples, key=lambda x: len(x[0]), reverse=True) 
        return [replace_by_tuple(text, replace_tuples) for text in texts]

    @classmethod
    def bring_forward_backward(cls) : 
        # 공통적으로 사용되는, 앞과 뒤에 들어갈 문자 유형
        return cls.filter_name_by_type(type_='forward'), cls.filter_name_by_type(type_='backward')


##################
# Tagger 상품정보 분석기
##################

class Tags(TagSets) : 

    @staticmethod
    def select(taglist, field) : 
        if field == '' : 
            return taglist
        elif field == 'name' : 
            return [tag.name for tag in taglist]
        elif field == 'type' : 
            return [tag.type for tag in taglist]
        elif field == 'replace_to' : 
            return [tag.replace_to for tag in taglist]
        elif field == 'add_on' :
            return [tag.add_on for tag in taglist]
        elif field == 'friend' : 
            return [tag.friend for tag in taglist]
        elif filed == 'enemy' : 
            return [tag.enemy for tag in taglist]
        else : 
            # print('>>>> TagSets error : field to filter does not exist')
            raise ValueError


    @classmethod
    def replace_to(cls, texts=[], type_=[], exact=False) : 
        if type_ == [] :
            replace_tuples = [(tag.name, tag.replace_to) for tag in cls()() if tag.replace_to != None]
        elif 'origin' in type_ : 
            replace_tuples = [(tag.name, tag.replace_to) for tag in cls()() if 'origin' in tag.friend.split(';')]
        else : 
            replace_tuples = [(tag.name, tag.replace_to) for tag in cls()() if tag.replace_to != None and tag.type in type_]

        replace_tuples = sorted(replace_tuples, key=lambda x: len(x[0]), reverse=True)
        replaced = [replace_by_tuple(text, replace_tuples, exact=exact) for text in texts]
        if 18 in type_ :
            # 원산지 태그의 경우 특수한 동의어 치환 적용
            replace_tuples = [(tag.name, tag.replace_to) for tag in cls()() if tag.replace_to != None and tag.type in type_]
            replaced_ = []
            for text in replaced : 
                # re.sub에서 찾은 것이 있으면 바꾼 동의어를, 없으면 원래 단어를 replaced에 넣기
                index = 1
                for i, j in replace_tuples: 
                    if i in text and index == 1 : 
                        replaced_.append(re.sub(r'^((.*:)|)((?:%s))(,.*|)$' % i, r'\1%s\4' % j, text))
                        index = 2
                if index == 1 :
                    replaced_.append(text)
            replaced = list(set(replaced_))
        return replaced


    @classmethod
    def filter_by_friend(cls, friend='') :
        def include(listA, element) : 
            if element in listA : 
                return True
            return False
        return [tag for tag in cls()() if friend in tag.friend.split(';')] 


class PrdTags(Tags) : 
    _instances = {}
    item = "prdtag"
    prior_tags = []
    type_only_for_final = [11]
    synonym_type_for_final = [11]


class MenuTags(Tags) : 
    _instances = {}
    item = "menutag"
    prior_tags = []
    type_only_for_final = [11]
    synonym_type_for_final = [0, 10, 11]