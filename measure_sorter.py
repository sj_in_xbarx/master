#-*- coding:utf-8 -*-
from __future__ import unicode_literals
import collections
import re
from operator import itemgetter

from . import tagtools
from .tagtools import Extractor
from .tag_sets import Measures


def extract_cliche(measures, cliche=['개', '팩', '박스']) : 
    extracted = []
    rest = []
    for i in measures : 
        for c in cliche : 
            if c in i and c != i : 
                extracted.append(i)
        if i not in extracted : 
            rest.append(i)
    return extracted, rest


def TempMeasure(x) : 
    MeasureClass = collections.namedtuple('MeasureClass',['value','len','str','num','char','cliche','cliche_len','none_cliche'])
    return MeasureClass(value=x, 
                        len=len(x), 
                        str=','.join(x), 
                        num=tagtools.find_num(x), 
                        char=tagtools.find_char(x),
                        cliche=extract_cliche(x)[0],
                        cliche_len=len(extract_cliche(x)[0]),
                        none_cliche=extract_cliche(x)[1]
                        )


def sort_larger_volume(volumes) :   
    # volume 중에서 큰 것대로 정렬하기 (g, kg, l, ml 비교) > (ml, L) > (kg, g) 순서
    volume_num_tuple = []
    ind_volume = []
    for v in volumes : 
        volume_nums = re.findall(r'([0-9]*[.]?[0-9]+)', v)
        if volume_nums == [] : 
            # kg, g 단이만 있으면 숫자 1을 추가하기
            v = '1' + v
            volume_nums = [1]

        if len(volume_nums) >= 2 :
            # 1kg ± 0.1kg 과 같은 형태는 후순위로 미뤄서 개체중량으로 되도록 처리하기
            ind_volume.append(v)

        if len(volume_nums) == 1 : 
            # volume과 weight가 유사한 경우 volume이 먼저 나오도록 1.2를 곱해줌(20%)
            if 'mL' in v :
                volume_num_tuple.append((v,float(volume_nums[0]) * 1.2))
            elif 'L' in v : 
                volume_num_tuple.append((v,float(volume_nums[0]) * 1000 * 1.2))
            elif 'kg' in v :
                volume_num_tuple.append((v,float(volume_nums[0]) * 1000))
            else : 
                volume_num_tuple.append((v,float(volume_nums[0])))

    volumes = sorted(volume_num_tuple, key=lambda element:element[1], reverse=True)
    sorted_volume = [v[0] for v in volumes] + ind_volume
    return sorted_volume


def if_volume_is_net_contents(rawname, master_unit) : 
    # (g|kg) / (개|팩|박스) 형태가 원래의 이름에 있으면 용량은 내용량이 됨. 
    bundle_char = re.findall(r'[\w]+/([\w]+)', master_unit)
    if bundle_char : 
        bundle_char = bundle_char[0]
    else : 
        bundle_char = tagtools.find_char(master_unit)
    # print('bundle char :::: ', bundle_char)
    bundle_rawname_map = {'팩': Measures.filter_name_by_type(type_='pack'),
                          '박스': Measures.filter_name_by_type(type_='box'),
                          '개': Measures.filter_name_by_type(type_='unit'),
                          }
    for i, j in bundle_rawname_map.items() : 
        # master_unit(주문단위)의 단위가 어떤 타입에 속하는지 찾기
        if bundle_char in j : 
            bundle_char = '|'.join(j)
    
    regex = '(?:g|kg)(?:{0})?/1?(?:{1})'.format('|'.join(Measures.filter_name_by_type(type_='backward')), bundle_char)
    volume_per_bundle = re.findall(r'%s' % regex, rawname.lower())
    if volume_per_bundle : 
        return True
    return False


def if_volume_is_ind_weight(rawname, volume, bundle_num, ind_num) : 
    # 조건 1. 0kg*0개, 0개*0kg 형태가 있어야 함
    regexes =  [volume.str + '[*]' + str(ind_num),
                volume.str + ' ' + str(ind_num),
                volume.str + '[/,]' + '(?:개|ea|입)',
                str(ind_num) + '(?:개|ea|입)' + '[*]' + volume.str,
                # str(ind_num) + '(?:개|ea|입)' + '[/,]' + '(?:팩|pac|박스|box)',
                ]
    volume_by_unit = []
    for regex_ in regexes : 
        volume_by_unit += re.findall(r'%s' % regex_.lower(), rawname.lower())
    if volume_by_unit : 
        return True
    return False



############################
'''
주문단위 표기법의 가능한 케이스
    n개
    n팩
    n박스
    n팩/박스
    1kg

'''
############################

def _sort_bundle(unit, box, pack, volume) : 
    # 주문단위에 3팩/박스 > (n)팩, (n)박스 형태가 가능하면 나오게 하기
    order_unit = '/'.join([i for i in [pack.str, box.str] if i])
    if order_unit : 
        return order_unit, unit.str

    # kg당 단가
    elif not unit.len and volume.str in ['kg', '1kg'] :
        return '1kg', ''

    # 수량만 있는 경우
    else : 
        return unit.str, ''


class ProductMeasureSorter : 

    def __init__(self, rawname='') : 
        self.rawname = rawname

    # unit(주문단위)과 ind_num(개체수량)을 정하는 규칙
    def sort_bundle(self, unit, box, pack, volume) : 
        # 팩이 여러개인 경우 개를 박스의 형태로 추가하기 (n팩/개 형태로 주문단위가 나오도록)
        self.unit, self.ind_num = _sort_bundle(unit, box, pack, volume)


    # net_contents(총중량, 내용량)와 ind_weight(개체중량)를 정하는 규칙
    def sort_volume(self, volume) :
        bundle_num, ind_num = map(tagtools.find_num, [self.unit, self.ind_num])
        # print(bundle_num, ind_num, volume.len)
        if volume.len == 1 :
            if ind_num > 1 :
                if if_volume_is_net_contents(self.rawname, self.unit) : 
                    # 00g/주문단위 형태가 있는 경우 총중량으로 처리하기
                    # print('volume is net contents')
                    self.net_contents = volume.str
                    self.ind_weight = tagtools.trim_volume(str(volume.num / (ind_num*bundle_num)) + volume.char)

                elif if_volume_is_ind_weight(self.rawname, volume, bundle_num, ind_num) : 
                    # print('volume is ind weight')
                    # volume*unit or unit/pack 형태가 있는 경우 용량을 개체용량으로 처리하기
                    self.net_contents = tagtools.trim_volume(str(volume.num * ind_num * bundle_num) + volume.char)
                    self.ind_weight = volume.str

                elif tagtools.areintext(['개', '팩', '박스'], self.unit) or self.unit == '' : 
                    # 개체수량이 여러개인 경우, volume을 개체수량으로 나누어 ind_num 만들기
                    # print('volume should be divided by ind num')
                    self.net_contents = tagtools.trim_volume(str(volume.num * bundle_num) + volume.char)
                    self.ind_weight = tagtools.trim_volume(str(volume.num / ind_num) + volume.char)
                else :
                    self.net_contents = volume.str
                    self.ind_weight = 'error'

            elif ind_num == 1 and self.unit in ['개', '팩', '박스', ''] :    
                # 수량이 1개인 경우, volume은 개체중량
                self.net_contents = tagtools.trim_volume(str(volume.num * bundle_num) + volume.char)
                self.ind_weight = volume.str
            
            else : 
                # 개체수량이 ''인 경우 volume을 개체중량으로 지정하지 않음
                self.net_contents = tagtools.trim_volume(str(volume.num * bundle_num) + volume.char)
                self.ind_weight = ''

        elif volume.len == 2 :
            large, small = sort_larger_volume(volume.value)
            large_num, small_num = map(tagtools.find_num, [large, small])
            large_char, small_char = map(tagtools.find_char, [large, small])

            if bundle_num >= 1 and ind_num > 1 : 
                # 개체수량이 있는 경우 작은 중량을 기준으로 net_contents를 재계산   
                self.net_contents = tagtools.trim_volume(str(small_num * bundle_num * ind_num) + small_char)
                self.ind_weight = tagtools.trim_volume(small)
            else : 
                self.net_contents = tagtools.trim_volume(large)
                self.ind_weight = tagtools.trim_volume(small)

        else : 
            self.net_contents = ''
            self.ind_weight = ''


    def sort_measure(self, unit, box, pack, volume, size) : 
        unit, box, pack, volume, size = list(map(TempMeasure, [unit, box, pack, volume, size]))
        # print(unit, box, pack, volume, size)
        # 분류하기
        self.sort_bundle(unit, box, pack, volume)
        self.sort_volume(volume)
        self.standard = size.str
        if self.unit == '' : 
            self.unit = '개'
        if self.ind_num == '개'  : 
            self.ind_num = ''
        if self.ind_weight == '' and (self.unit != '박스' or self.ind_num ) :
            if self.net_contents != '' and tagtools.find_num(self.unit) * tagtools.find_num(self.ind_num) == 1 : 
                self.ind_weight = self.net_contents

        return self.__dict__

