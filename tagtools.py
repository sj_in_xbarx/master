#-*- coding:utf-8 -*-
from __future__ import unicode_literals
import re

##################
# string 다루기에 필요한 것
##################


def isinstance_str(string) : 
    # checking string compatible on python 2.X and 3.X
    if isinstance(string, str) : 
        return True
    try :
        if isinstance(string, unicode) :
            return True
    except : 
        return False
    return False


def replace_by_dict(text, dic): 
    # dictionary 기반의 동의어 변환기
    for i, j in dic.items(): 
        if i != '' and j != None : 
            text = text.replace(i, j)
    return text


def del_one(text) :  
    # 단위 앞의 숫자가 1인 경우 1 지우기
    def del_one_(text) : 
        search_obj = re.search(r'^([^a-zA-Z가-힣]+)([a-zA-Z가-힣]*)$', text)
        if search_obj != None : 
            if search_obj.group(1) == '1' :
                text = search_obj.group(2)
        return text

    if isinstance_str(text) : 
        return del_one_(text)

    elif isinstance(text, list) : 
        return list(set([del_one_(t) for t in text]))


def float_to_int(num) : 
    decimal_place = re.findall(r'\.\d+', str(num))
    if decimal_place : 
        if len(decimal_place[0]) <= 4 and decimal_place[0] != '.0' : 
            return num
    if num < 1 : 
        return round(num, 3)
    if round(num, 2) == int(num) : 
        return int(num)
    else : 
        return round(num, 1)


def find_num(text) : 
    if text == [] or text == '' : 
        return 1

    if isinstance(text, list) and len(text) >= 1 : 
        text = text[0]

    if isinstance_str(text) : 
        text = text.strip('.')
        numbers = re.findall(r'[\d]*[.]*[\d]+', text)

        if len(numbers) == 1 : 
            return float_to_int(float(numbers[0]))

        elif len(numbers) >= 2 :
            # 숫자가 두 번 나오는 경우 (개 * 개)가 아니면 앞의 숫자를 return하기
            multi_numbers = re.findall(r'([\d]+)개[*]([\d]+)개', text)
            if len(multi_numbers) == 1 :
                return int(multi_numbers[0][0]) * int(multi_numbers[0][1])
            else : 
                # 000단위 +- n단위 등, 두 번째 숫자는 범위를 나타내는 경우가 많아 앞의 숫자를 반환
                return float_to_int(float(numbers[0]))
        return 1

    else : 
        # print('Error : failed to find_num', text)
        raise ValueError


def find_char(text) : 
    if text == [] or text == '' : 
        return ''

    if isinstance(text, list) and len(text) >= 1 : 
        text = text[0]

    if isinstance_str(text) : 
        chars = re.findall(r'[.~\d]+(\w+)', text)
        if len(chars) == 1 : 
            return chars[0]
        return text

    else : 
        # print('Error : failed to find_char', text)
        raise ValueError    


def del_duplicated_num(values) : 
    # 숫자가 같은 것이 두 개 있으면 하나만 남기기
    values = [(value, find_num(value)) for value in values]
    filtered_value = []
    filtered_num = []
    for value in values : 
        if value[1] not in filtered_num : 
            filtered_value.append(value[0])
            filtered_num.append(value[1])
    return filtered_value


def find_front_char(value) : 
    front_char = re.findall(r'([가-힣a-z]+)[\s]?[\d]+', value)
    if front_char : 
        return front_char[0]
    return ''


def trim_volume(value) : 
    # 내용량의 표기가 1 미만의 L/kg인 경우 ml/g으로 바꾸고, 1000 이상의 ml/g은 L/kg으로 바꾸기
    volume_measure = ['kg', 'g', 'mL', 'L']
    # 범주형 숫자가 등장한 경우 skip하고 원래 값을 반환함
    for scale_mark in ['~', '-'] : 
        if scale_mark in value : 
            return value



    value = value.replace(',','')
    number = float(find_num(value))
    char = ''
    for m in volume_measure : 
        if m in value : 
            char = m
            break
    front_char = find_front_char(value)

    if not char : 
        # 중량이 정형화되지 않는 경우 원래의 값 반환
        return value

    if number >= 1000 : 
        if char == 'mL' : 
            return front_char + str(float_to_int(number / 1000)) + "L"
        elif char == 'g' :
            return front_char + str(float_to_int(number / 1000)) + "kg"

    elif number < 1 : 
        if char == 'L' : 
            return front_char + str(float_to_int(number * 1000)) + "mL"
        elif char == 'kg' : 
            return front_char + str(float_to_int(number * 1000)) + "g"
            
    return front_char + str(float_to_int(number)) + char


def areintext(words, text_to_find) : 
    for word in words : 
        if word in text_to_find : 
            return True
    return False


def reorder_name(product_name, sps=[".", "&", "*", "#", "-", "%", "~", "'", "/", "+"]) :
    # 괄호 안에 있는 단어가 뒤에 위치하도록 : 괄호가 앞에 있거나 뒤에 있거나 모두 해당
    move_backward = []
    in_parenthesis = re.findall(r'\(([\d\w]*)\)', product_name)
    if in_parenthesis : 
        for i in in_parenthesis :
            if i :
                product_name = re.sub(r'\((?:%s)\)' % i, '', product_name, 1)
                move_backward.append(i)

     # 상품 이름에서 특수문자들을 기준으로 문자열을 나눈 다음 list로 만들기
    regexStr = re.compile('[가-힣\d\w%s]+' % "".join(map(lambda x: "\\%s"%x, sps)))
    product_name_listed = regexStr.findall(product_name)

    product_name_listed += move_backward
    return ','.join(product_name_listed)



##################
# measure의 정규표현식 생성에 필요한 것
##################

def join_to_regex(terms_list, add='') : 
    # 단어를 |로 join해서 찾는 정규표현식 만들기
    terms_list.sort(key=lambda x : len(x), reverse=True)
    return '(?:{text}){add}'.format(text='|'.join(terms_list), add=add)


class Extractor() : 

    def __init__(self, keywords, join=False, forward=None, backward=None, avoid_front='', avoid_back='') : 
        if not isinstance(keywords, list) :
            keywords = [keywords]
        self.keywords = keywords

        if join : 
            self.join_by_bar()

        if forward != None or backward != None : 
            self.add_common_num(forward, backward)

        if avoid_front != '' or avoid_back != '' : 
            if avoid_front == '' : 
                avoid_front = ['num','kor','eng']
            elif avoid_back == '' : 
                avoid_front = ['num','kor','eng']
            self.avoid(avoid_front, avoid_back)

    def join_by_bar(self, add='') : 
        self.keywords = [join_to_regex(self.keywords, add)]

    def add_common_num(self, forward='', backward='') : 
        # 흔하게 숫자가 붙여지는 형태의 정규표현식 만들기. forward, backward는 필요한 경우 앞뒤로 추가 가능. 
        if isinstance(forward, list) : 
            forward = join_to_regex(forward, '?[\s]?')
        if isinstance(backward, list) : 
            backward = join_to_regex(backward, '?')
                
        added = []
        for body in self.keywords : 
            added.append("(" + forward + "[\.0-9]+[가-힣]*[~∼±\-]+[\.0-9]+" + body + backward + ")")
            added.append("(" + forward + "[\d]+[\.][\d]+" + body + backward + ")")
            added.append("(" + forward + "[\d]+" + body + backward + ")")
        self.keywords = added

    # pprefix, ppostfix  divider >> prohibit approach
    def avoid(self, front=['num','kor','eng'], back=['num','kor','eng']) : 
        # front와 back에는 각각 앞과 뒤에 오면 안되는(re.find로 찾을 때 구분할 기준) 문자타입 입력

        def build_wall(params, forward=False) : 
        # 'num', 'kor', 'eng' 가 parameter에 포함되면 divider로 추가하기
            if forward : 
                position = '^'
            else : 
                position = '$'

            divider = ''
            div_preset = {'num': '0-9', 'kor': '가-힣', 'eng': 'a-zA-Z'}
            for keyword in params : 
                if keyword in div_preset.keys() : 
                    divider += div_preset[keyword]
                else : 
                    divider += keyword

            if divider == '' : 
                return '()'
            else : 
                return '([^&' + divider + ']|' + position + ')'  

        # 리스트 안의 텍스트가 divider 추가하기 전에 미리 () 안에 들어가있어야 함 ")"
        regexes = []
        for regex in self.keywords : 
            if (regex[0] != "(" or regex.startswith('(?:')) and not regex.endswith('+)') : 
                regexes.append("(" + regex + ")")
            else : 
                regexes.append(regex)
        self.keywords = [build_wall(front, forward=True) + regex + build_wall(back) for regex in regexes]

    def del_and_gather(self, name, except_=None) : 
        # 찾고자 하는 string을 ()로 감싼 다음 앞, 뒤의 divider도 각각 그룹으로 넣어야 함 >>> sub를 \1 \3 으로 적용
        gather = []
        for regex in self.keywords : 
            # print(regex[:300])
            extractor = re.compile(regex)
            for i in range(3) :         # 구분자를 사이에 두고 겹쳐있는 항목에 대비해 3번 돌리기
                group = extractor.findall(name)
                # print(group)
                if group :
                    assert len(group[0]) == 3, (
                        "divider or package to find {target} is not proper : {regex}".format(target=group, regex=regex))
                    extracted = group[0][1]
                    if extracted : 

                        if except_ :        # 태깅을 강제로 안하고 싶은 문구가 있는 경우 str이거나 list일때 각각 처리
                            if isinstance(except_, str) and except_ in extracted : 
                                extracted = extracted.replace(except_, '')
                                name += ',' + except_
                            
                            elif isinstance(except_, list) : 
                                for word_ in except_ :
                                    if word_ in extracted : 
                                        extracted = extracted.replace(word_, '')
                                        name += ',' + word_

                        gather.append(extracted.strip('/').strip(','))
                        name = extractor.sub(r'\1 \3', name, 1)

        return (name, list(set(gather)))

    def __add__(self, other) : 
        return Extractor(self.keywords + other.keywords)

    def __radd__(self, other) : 
        return other + self

    def __str__(self) : 
        return 'RegexBuilder : \n' + '\n'.join(self.keywords) + '\n'