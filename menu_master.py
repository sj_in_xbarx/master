#-*- coding:utf-8 -*-
from __future__ import unicode_literals

import re
import multiprocessing
import collections

from . import tag_sets
from . import tag_finder
from . import tag_sorter
from . import tagtools

# if value is empty, set as None
def setNone(dict) :    
    for k, v in dict.items() : 
        if not v : 
            dict[k] = None
        elif isinstance(v, list) : 
            dict[k] = ','.join(v)
    return dict


def convert_mastermenu(rawname, menu_tags=None) :
    # 메뉴 이름을 string으로 넣으면 dictionary로 마스터 변환 결과를 반환하는 함수
    if menu_tags != None : 
        tag_sets.MenuTags()(menu_tags)

    if rawname : 
        # rawname의 특수문자와 띄어쓰기를 , 로 변환하기
        regexStr = re.compile('[가-힣\d\w\s\(\)\.]+')
        menu_name = regexStr.findall(rawname)
        if not menu_name : 
            return {}
        
        # tag를 찾고 분류하기
        rest_name, tags_found = tag_finder.MenuTagFinder(','.join(menu_name).upper()).find_tag()
        info_found = tag_sorter.MenuTagSorter(tags_found).__dict__
        info_found['rawname'] = rawname
        info_found['rest'] = rest_name.strip(',').strip()
        info_found['menu_cate'] = info_found.pop('product_cate', None)
        info_found['menu_type'] = info_found.pop('product_type', None)
        for key in ['taglist', 'suffix']:
            info_found.pop(key, None)
        return setNone(info_found)
    return {}


# input is list of rawnames, return list of dictionaries
def convert_bulk_menunames(list_of_menu_names, menu_tags=None) : 
    tag_sets.MenuTags()(menu_tags)
    result = list(map(convert_mastermenu, list_of_menu_names))
    return result    


# input should be dictionary, including 'rawname' as key. return updated dictionary.
def convert_mastermenu_from_dict(dict_input, menu_tags=None) : 
    if 'rawname' in dict_input.keys() : 
    # dictionary list를 넣으면 해당 dictionary 안에 마스터 변환 결과를 추가해서 반환하는 함수
        dict_input.update(convert_mastermenu(dict_input['rawname'], menu_tags))
    elif 'menu_name' in dict_input.keys() : 
        dict_input.update(convert_mastermenu(dict_input['menu_name'], menu_tags))
    return dict_input



##################################
###### USE ON PYTHON 3 ONLY ######
##################################

def convert_mastermenu_bulk(json_input, menu_tags=None) :
    # dictionary list를 넣으면 해당 dictionary 안에 마스터 변환 결과를 추가해서 반환하는 함수 - multiprocessing
    tag_sets.MenuTags()(menu_tags)
    if isinstance(json_input, dict) :
        json_input = [json_input]
    pool = multiprocessing.Pool(processes=4)
    result = pool.map(convert_mastermenu_from_dict, json_input)
    pool.close()
    return result
